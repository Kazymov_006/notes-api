import pytest
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from notes_api.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def enable_db_for_all_tests(db):
    pass


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def auth_api_client(user):
    api_client = APIClient()
    token, created = Token.objects.get_or_create(user=user)
    api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
    return api_client


@pytest.fixture
def user():
    return UserFactory()
