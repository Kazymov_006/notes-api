from django.utils.decorators import method_decorator
from rest_framework import (
    mixins,
    permissions,
    generics,
    status,
    authentication,
)
from rest_framework.decorators import api_view, action, permission_classes
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from .models import Note, Category, User
from .serializers import (
    NoteSerializer,
    NoteViewSerializer,
    CategoryViewSerializer,
    CategorySerializer,
    RegisterSerializer,
)


@swagger_auto_schema(method="get", operation_id="api_status")
@api_view()
@permission_classes([permissions.AllowAny])
def main_api(request):
    """API availability checking method"""
    return Response({"status": "ok"})


@method_decorator(name="post", decorator=swagger_auto_schema(tags=["auth"]))
class RegisterView(generics.CreateAPIView):
    model = User
    permission_classes = [permissions.AllowAny]
    serializer_class = RegisterSerializer


class LogoutView(APIView):
    @swagger_auto_schema(
        responses={200: "Logged out"},
        tags=["auth"],
    )
    def get(self, request, format=None):
        # simply delete the token to force a login
        try:
            request.user.auth_token.delete()
        except User.auth_token.RelatedObjectDoesNotExist:
            pass
        return Response(status=status.HTTP_200_OK)


@method_decorator(name="create", decorator=swagger_auto_schema(tags=["notes"]))
@method_decorator(
    name="retrieve", decorator=swagger_auto_schema(tags=["notes"])
)
@method_decorator(name="update", decorator=swagger_auto_schema(tags=["notes"]))
@method_decorator(
    name="partial_update", decorator=swagger_auto_schema(tags=["notes"])
)
@method_decorator(name="list", decorator=swagger_auto_schema(tags=["notes"]))
@method_decorator(
    name="destroy", decorator=swagger_auto_schema(tags=["notes"])
)
class NotesView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    GenericViewSet,
):
    """Notes"""

    authentication_classes = [
        authentication.SessionAuthentication,
        authentication.TokenAuthentication,
    ]

    permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        # for POST, PUT, PATCH and OPTIONS requests
        if self.action in ("create", "update", "partial_update", "metadata"):
            return NoteSerializer
        # for GET requests, includes nested serializers and readonly fields
        if self.action in ("list", "retrieve"):
            return NoteViewSerializer

    def get_queryset(self):
        user = self.request.user
        return Note.objects.all().select_related("author").filter(author=user)

    @swagger_auto_schema(
        method="get",
        responses={200: CategoryViewSerializer(many=True)},
        tags=["notes"],
    )
    @action(["GET"], detail=True)
    # /api/notes/<id>/categories
    def categories(self, request, *args, **kwargs):
        """Get the list of categories related to this note"""
        object: Note = self.get_object()
        categories_ = object.categories.all()
        serializer = CategoryViewSerializer(categories_, many=True)
        return Response(serializer.data)


@method_decorator(
    name="create", decorator=swagger_auto_schema(tags=["categories"])
)
@method_decorator(
    name="retrieve", decorator=swagger_auto_schema(tags=["categories"])
)
@method_decorator(
    name="update", decorator=swagger_auto_schema(tags=["categories"])
)
@method_decorator(
    name="partial_update", decorator=swagger_auto_schema(tags=["categories"])
)
@method_decorator(
    name="list", decorator=swagger_auto_schema(tags=["categories"])
)
@method_decorator(
    name="destroy", decorator=swagger_auto_schema(tags=["categories"])
)
class CategoriesView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    GenericViewSet,
):
    """Categories"""

    authentication_classes = [
        authentication.SessionAuthentication,
        authentication.TokenAuthentication,
    ]

    permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        # for POST, PUT, PATCH and OPTIONS requests
        if self.action in ("create", "update", "partial_update", "metadata"):
            return CategorySerializer
        # for GET requests, includes nested serializers and readonly fields
        if self.action in ("list", "retrieve"):
            return CategoryViewSerializer

    def get_queryset(self):
        user = self.request.user
        return (
            Category.objects.all().select_related("author").filter(author=user)
        )

    @swagger_auto_schema(
        method="get",
        responses={200: NoteViewSerializer(many=True)},
        tags=["categories"],
    )
    @action(["GET"], detail=True)
    # /api/categories/<id>/notes
    def notes(self, request, *args, **kwargs):
        """Get the list of categories related to this note"""
        object: Category = self.get_object()
        notes_ = object.notes.all()
        serializer = NoteViewSerializer(notes_, many=True)
        return Response(serializer.data)
