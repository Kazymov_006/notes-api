from django.contrib import admin

from .models import Note, Category


class CategoriesInline(admin.TabularInline):
    model = Note.categories.through
    fields = (
        "id",
        "category",
    )
    readonly_fields = ("id",)


class NotesInline(admin.TabularInline):
    model = Category.notes.through
    fields = (
        "id",
        "note",
    )
    readonly_fields = ("id",)


@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "slug",
        "author",
        "content_type",
    )
    list_display_links = (
        "id",
        "slug",
    )
    list_filter = ("author", "content_type")
    search_fields = (
        "id",
        "slug",
        "author",
        "content_type",
    )
    fields = (
        "id",
        "title",
        "slug",
        "author",
        "content_type",
        "content",
    )
    readonly_fields = (
        "id",
        "slug",
    )
    inlines = (CategoriesInline,)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "slug",
        "author",
    )
    list_display_links = (
        "id",
        "slug",
    )
    list_filter = ("author",)
    search_fields = (
        "id",
        "slug",
        "author",
    )
    fields = (
        "id",
        "title",
        "slug",
        "author",
        "description",
    )
    readonly_fields = (
        "id",
        "slug",
    )
    inlines = (NotesInline,)
