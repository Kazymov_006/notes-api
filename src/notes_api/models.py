from autoslug import AutoSlugField
from django.db import models
from django.contrib.auth.models import User

from .enums import ContentType


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Category(BaseModel):
    title = models.CharField(
        max_length=200, default="Untitled", verbose_name="Title"
    )
    slug = AutoSlugField(
        populate_from="title", verbose_name="Slug", always_update=True
    )
    description = models.CharField(
        max_length=400, blank=True, verbose_name="Description"
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Author",
        related_name="categories",
    )

    def __str__(self):
        return (
            f"Category({self.pk}) - {self.slug}, "
            f"author - {self.author.email}"
        )

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Note(BaseModel):
    title = models.CharField(
        max_length=200, default="Untitled", verbose_name="Title"
    )
    slug = AutoSlugField(
        populate_from="title",
        unique_with="id",
        verbose_name="Slug",
        always_update=True,
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Author",
        related_name="notes",
    )
    content = models.TextField(blank=True, null=True, verbose_name="Content")
    content_type = models.CharField(
        max_length=10,
        choices=ContentType.choices,
        verbose_name="Content type",
        default=ContentType.TEXT,
    )
    categories = models.ManyToManyField(Category, related_name="notes")

    def __str__(self):
        return (
            f"Note({self.pk}) - {self.slug}, " f"author - {self.author.email}"
        )

    class Meta:
        verbose_name = "Note"
        verbose_name_plural = "Notes"
