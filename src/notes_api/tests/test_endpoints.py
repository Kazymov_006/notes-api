from rest_framework import status


def test_login_with_incorrect_credentials(api_client):
    # Invalid credentials
    response = api_client.post(
        "/api/token/",
        data={"username": "asdfdadfadf", "password": "incorrect_password"},
        format="json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.json()


def test_register_and_login_with_correct_credentials(api_client):
    # Register NewTestUser
    registration_data = {
        "username": "NewTestUser",
        "email": "test@test.com",
        "password": "Asdasd123",
    }
    response = api_client.post(
        "/api/register/", data=registration_data, format="json"
    )
    assert response.status_code == status.HTTP_201_CREATED, response.json()
    result = response.json()
    assert result["email"] == registration_data["email"], response.json()
    assert result["username"] == registration_data["username"], response.json()
    # Login with this creds
    response = api_client.post(
        "/api/token/",
        data={"username": "NewTestUser", "password": "Asdasd123"},
        format="json",
    )
    assert response.status_code == status.HTTP_200_OK, response.json()
    assert "token" in response.json(), response.json()


def test_authenticated_user_gets_categories(auth_api_client):
    response = auth_api_client.get("/api/categories/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_unauthenticated_user_gets_categories(api_client):
    response = api_client.get("/api/categories/")
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.json()


def test_authenticated_user_gets_notes(auth_api_client):
    response = auth_api_client.get("/api/notes/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_unauthenticated_user_gets_notes(api_client):
    response = api_client.get("/api/notes/")
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.json()


def test_authenticated_user_gets_main_page(auth_api_client):
    response = auth_api_client.get("/api/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_unauthenticated_user_gets_main_page(api_client):
    response = api_client.get("/api/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_authenticated_user_posts_puts_and_patches_categories(auth_api_client):
    data_to_post = {
        "title": "TestCat1",
        "description": "TestCat1",
        "notes": [],
    }
    response = auth_api_client.post(
        "/api/categories/", data_to_post, format="json"
    )
    resp_data = response.json()
    assert response.status_code == status.HTTP_201_CREATED, resp_data
    assert resp_data["title"] == data_to_post["title"], resp_data["title"]
    assert resp_data["description"] == data_to_post["description"], resp_data[
        "description"
    ]
    assert resp_data["notes"] == data_to_post["notes"], resp_data["notes"]
    prev_slug = resp_data["slug"]
    assert prev_slug != "", prev_slug

    # PUT, PATCH, GET detail, and GET action /notes/ testing
    posted_cat_id = resp_data["id"]

    # GET detail
    response = auth_api_client.get(f"/api/categories/{posted_cat_id}/")
    assert response.status_code == status.HTTP_200_OK, response.json()

    # PUT
    data_to_put = {
        "title": "TestCat1Changed",
        "description": "TestCat1Changed",
        "notes": [],
    }
    response = auth_api_client.put(
        f"/api/categories/{posted_cat_id}/", data_to_put, format="json"
    )
    resp_data = response.json()
    assert response.status_code == status.HTTP_200_OK, resp_data
    assert resp_data["title"] == data_to_put["title"], resp_data["title"]
    assert resp_data["description"] == data_to_put["description"], resp_data[
        "description"
    ]
    assert resp_data["notes"] == data_to_put["notes"], resp_data["notes"]
    assert resp_data["slug"] != "", resp_data["slug"]
    # Test slug updated
    assert resp_data["slug"] != prev_slug

    # PATCH
    data_to_patch = {"description": "This is new description!"}
    response = auth_api_client.patch(
        f"/api/categories/{posted_cat_id}/", data=data_to_patch, format="json"
    )
    resp_data = response.json()
    assert response.status_code == status.HTTP_200_OK, resp_data
    assert resp_data["title"] == data_to_put["title"], resp_data["title"]
    assert resp_data["description"] == data_to_patch["description"], resp_data[
        "description"
    ]
    assert resp_data["notes"] == data_to_put["notes"], resp_data["notes"]

    # GET action /notes/
    response = auth_api_client.get(f"/api/categories/{posted_cat_id}/notes/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_authenticated_user_posts_puts_and_patches_notes(auth_api_client):
    data_to_post = {
        "title": "TestNote1",
        "content_type": "text",
        "content": "This is test note.",
        "categories": [],
    }
    response = auth_api_client.post("/api/notes/", data_to_post, format="json")
    resp_data = response.json()
    assert response.status_code == status.HTTP_201_CREATED, resp_data
    assert resp_data["title"] == data_to_post["title"], resp_data["title"]
    assert (
        resp_data["content_type"] == data_to_post["content_type"]
    ), resp_data["content_type"]
    assert resp_data["content"] == data_to_post["content"], resp_data[
        "content"
    ]
    assert resp_data["categories"] == data_to_post["categories"], resp_data[
        "categories"
    ]
    prev_slug = resp_data["slug"]
    assert prev_slug != "", prev_slug

    # PUT, PATCH, GET detail, and GET action /notes/ testing
    posted_cat_id = resp_data["id"]

    # GET detail
    response = auth_api_client.get(f"/api/notes/{posted_cat_id}/")
    assert response.status_code == status.HTTP_200_OK, response.json()

    # PUT
    data_to_put = {
        "title": "TestNote1Changed",
        "content_type": "text",
        "content": "This is changed test note.",
        "categories": [],
    }
    response = auth_api_client.put(
        f"/api/notes/{posted_cat_id}/", data_to_put, format="json"
    )
    resp_data = response.json()
    assert response.status_code == status.HTTP_200_OK, resp_data
    assert resp_data["title"] == data_to_put["title"], resp_data["title"]
    assert resp_data["content_type"] == data_to_put["content_type"], resp_data[
        "content_type"
    ]
    assert resp_data["content"] == data_to_put["content"], resp_data["content"]
    assert resp_data["categories"] == data_to_put["categories"], resp_data[
        "categories"
    ]
    assert resp_data["slug"] != "", resp_data["slug"]
    # Test slug updated
    assert resp_data["slug"] != prev_slug

    # PATCH
    data_to_patch = {"content": "Completely new note text."}
    response = auth_api_client.patch(
        f"/api/notes/{posted_cat_id}/", data=data_to_patch, format="json"
    )
    resp_data = response.json()
    assert response.status_code == status.HTTP_200_OK, resp_data
    assert resp_data["title"] == data_to_put["title"], resp_data["title"]
    assert resp_data["content_type"] == data_to_put["content_type"], resp_data[
        "content_type"
    ]
    assert resp_data["content"] == data_to_patch["content"], resp_data[
        "content"
    ]
    assert resp_data["categories"] == data_to_put["categories"], resp_data[
        "categories"
    ]

    # GET action /notes/
    response = auth_api_client.get(f"/api/notes/{posted_cat_id}/categories/")
    assert response.status_code == status.HTTP_200_OK, response.json()


def test_different_users_categories_and_notes(api_client):
    # Register two different users
    registration_data = {
        "username": "NewTestUser1",
        "email": "test1@test.com",
        "password": "Asdasd123",
    }
    api_client.post("/api/register/", data=registration_data, format="json")
    response = api_client.post(
        "/api/token/",
        data={"username": "NewTestUser1", "password": "Asdasd123"},
        format="json",
    )
    token = response.json()["token"]
    headers1 = {"HTTP_AUTHORIZATION": f"Token {token}"}

    registration_data = {
        "username": "NewTestUser2",
        "email": "test2@test.com",
        "password": "Asdasd123",
    }
    api_client.post("/api/register/", data=registration_data, format="json")
    response = api_client.post(
        "/api/token/",
        data={"username": "NewTestUser2", "password": "Asdasd123"},
        format="json",
    )
    token = response.json()["token"]
    headers2 = {"HTTP_AUTHORIZATION": f"Token {token}"}

    # POST SOME CATEGORIES
    # from first user
    user1_categories_ids = []

    data_to_post = {
        "title": "TestCat1",
        "description": "TestCat1",
        "notes": [],
    }
    response = api_client.post(
        "/api/categories/", data=data_to_post, **headers1
    )
    user1_categories_ids.append(response.json()["id"])

    data_to_post = {
        "title": "TestCat2",
        "description": "TestCat2",
        "notes": [],
    }
    response = api_client.post(
        "/api/categories/", data=data_to_post, **headers1
    )
    user1_categories_ids.append(response.json()["id"])

    # from second user
    user2_categories_ids = []

    data_to_post = {
        "title": "TestCat1",
        "description": "TestCat1",
        "notes": [],
    }
    response = api_client.post(
        "/api/categories/", data=data_to_post, **headers2
    )
    user2_categories_ids.append(response.json()["id"])

    data_to_post = {
        "title": "TestCat2",
        "description": "TestCat2",
        "notes": [],
    }
    response = api_client.post(
        "/api/categories/", data=data_to_post, **headers2
    )
    user2_categories_ids.append(response.json()["id"])

    # TEST POST NOTES WITH CATEGORIES FROM DIFFERENT USERS
    data_to_post = {
        "title": "TestNote1",
        "content_type": "text",
        "content": "This is test note.",
        "categories": user1_categories_ids + user2_categories_ids,
    }
    response = api_client.post(
        "/api/notes/", data_to_post, format="json", **headers1
    )
    resp_data = response.json()
    # test that only first users' categories were added
    assert resp_data["categories"] == user1_categories_ids, resp_data[
        "categories"
    ]


def test_non_existing_page(api_client):
    response = api_client.get("/non/existing/page/")
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_swagger_and_redoc_access_for_unauthenticated_user(api_client):
    response = api_client.get("/api/swagger/")
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = api_client.get("/api/redoc/")
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_swagger_and_redoc_access_for_non_admin_user(auth_api_client):
    response = auth_api_client.get("/api/swagger/")
    assert response.status_code == status.HTTP_403_FORBIDDEN
    response = auth_api_client.get("/api/redoc/")
    assert response.status_code == status.HTTP_403_FORBIDDEN
