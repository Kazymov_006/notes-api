from django.db import models


class ContentType(models.TextChoices):
    TEXT = "text", "Text"
    TODO_LIST = "todo", "Todo list"
