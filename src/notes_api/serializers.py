from rest_framework import serializers

from .models import Note, User, Category


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data["username"],
            email=validated_data["email"],
            password=validated_data["password"],
        )
        return user

    class Meta:
        model = User
        fields = ("id", "username", "email", "password")


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email")
        extra_kwargs = {"email": {"help_text": "Электронная почта"}}


class NoteSerializer(serializers.ModelSerializer):
    author_id = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    categories = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Category.objects.all()
    )

    def validate(self, attrs):
        if "categories" in attrs:
            if "author_id" not in attrs:
                attrs["categories"] = []
                return attrs
            # note and category must have the same author
            author_id_ = attrs["author_id"].id
            attrs["categories"] = [
                category
                for category in attrs["categories"]
                if category.author_id == author_id_
            ]
        return attrs

    def create(self, validated_data):
        validated_data["author_id"] = validated_data["author_id"].id
        return super(NoteSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if "author_id" in validated_data:
            validated_data["author_id"] = validated_data["author_id"].id
        return super(NoteSerializer, self).update(instance, validated_data)

    class Meta:
        model = Note
        fields = (
            "id",
            "title",
            "slug",
            "content_type",
            "content",
            "author_id",
            "categories",
            "created_at",
            "updated_at",
        )
        extra_kwargs = {"slug": {"read_only": True}}


class CategorySerializer(serializers.ModelSerializer):
    author_id = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    notes = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Note.objects.all()
    )

    def validate(self, attrs):
        if "notes" in attrs:
            if "author_id" not in attrs:
                attrs["notes"] = []
                return attrs
            # note and category must have the same author
            author_id_ = attrs["author_id"].id
            attrs["notes"] = [
                note for note in attrs["notes"] if note.author_id == author_id_
            ]
        return attrs

    def create(self, validated_data):
        validated_data["author_id"] = validated_data["author_id"].id
        return super(CategorySerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if "author_id" in validated_data:
            validated_data["author_id"] = validated_data["author_id"].id
        return super(CategorySerializer, self).update(instance, validated_data)

    class Meta:
        model = Category
        fields = (
            "id",
            "title",
            "slug",
            "author_id",
            "description",
            "notes",
            "created_at",
            "updated_at",
        )
        extra_kwargs = {"slug": {"read_only": True}}


class NoteViewSerializer(NoteSerializer):
    author = UserSerializer(read_only=True)
    categories = CategorySerializer(many=True)

    class Meta:
        model = NoteSerializer.Meta.model
        fields = (
            *NoteSerializer.Meta.fields,
            "author",
        )
        extra_kwargs = NoteSerializer.Meta.extra_kwargs


class CategoryViewSerializer(CategorySerializer):
    author = UserSerializer(read_only=True)
    notes = NoteSerializer(many=True)

    class Meta:
        model = CategorySerializer.Meta.model
        fields = (
            *CategorySerializer.Meta.fields,
            "author",
        )
        extra_kwargs = CategorySerializer.Meta.extra_kwargs


class UserViewSerializer(UserSerializer):
    notes = NoteSerializer(many=True, read_only=True)
    categories = CategorySerializer(many=True, read_only=True)
