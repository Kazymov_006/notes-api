# Test task for "Technokratos"
## Setup instructions
### develop
- `git clone https://gitlab.com/Kazymov_006/notes-api.git` - 
  clone repo
- `pip install -r requirements.txt` - install requirements (poetry)
- `poetry shell` - enter a virtual environment
- `poetry install` - install required modules
- `export $(cat .env.dev | egrep -v "(^#.*|^$)" | xargs)` - export environment 
  variables
- `docker-compose -f docker-compose.dev.yml up -d` - run postgres inside a docker container
- `cd src` - switch to app's root directory
- `python manage.py migrate` - run database migrations
- `pytest --cov` - run test and check coverage(97%) (optional)
- `python manage.py runserver` - run server
- `cd ..` return to repo's root directory
- `docker-compose -f docker-compose.dev.yml down --volumes` -down container 
  and remove database, so if in prod version another db is used it won't 
  conflict with each other 

### production
- `git clone https://gitlab.com/Kazymov_006/notes-api.git` - clone repo
- `docker-compose -f docker-compose.prod.yml up --build` - run server inside a 
  docker container, if it was already built, do not use flag `--build`

## Description
This is my solution for test task for selection for an internship
### Documentation pathes
Note that you have to be authorized administrator for accessing docs. \
You can create admin with following command: \
`[src]$ python manage.py createsuperuser`

- `/api/swagger/` - swagger
- `/api/redoc/` - redoc

## Progress
- [x] create docker-compose file for postgresql service
- [x] create backbone server and notes application
- [x] create models
- [x] create viewset for notes
- [x] create viewset for categories
- [x] configure authwntication
- [x] create registration
- [x] create tests for api
- [x] create prod docker-compose file
